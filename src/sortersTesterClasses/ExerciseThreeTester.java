package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import sorterClasses.BubbleSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;
import interfaces.Sorter;
import sortersTesterClasses.*;

public class ExerciseThreeTester {
	public static void main(String[] args) {
		
		Sorter<Integer> sorter = new BubbleSortSorter<Integer>();
		String OGArr, IncArr, DecArr; OGArr = IncArr = DecArr = "{ ";

		Integer numArray[] = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
		System.out.println("The original array is: \n");
		printArray(OGArr, numArray);
		
		sorter.sort(numArray, new IntegerComparator1());
		System.out.println("The increasing array is: \n");
		printArray(IncArr, numArray);
		
		sorter.sort(numArray, new IntegerComparator2());
		System.out.println("The decreasing array is: \n");
		printArray(DecArr, numArray);
		
	}
	
	public static void printArray(String strArr, Integer numArray[]) {
		for(int i = 0; i<numArray.length; i++) {
			strArr += numArray[i] + " ";
		}
		strArr += "}";
		System.out.println(strArr + "\n");
	}



}
