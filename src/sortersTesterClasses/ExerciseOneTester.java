package sortersTesterClasses;

import sorterClasses.*;
import sortersTesterClasses.*;
import java.util.Random;


public class ExerciseOneTester {
	
	private static BubbleSortSorter<Entero> sorter = new BubbleSortSorter<Entero>(); 
	private static Entero array[] = new Entero[20];
	private static Random rand = new Random();

	public static void main(String[] args) {
		for(int i = 0; i < array.length; i++) array[i] = new Entero(rand.nextInt(100));
		String OGArr= "{ ";
		
		System.out.println("The original array is: \n");
		printArray(OGArr, array);
		
		sorter.sort(array, null);
		System.out.println("The sorted array is: \n");
		printArray(OGArr, array);

	}
	
	public static void printArray(String strArr, Entero numArray[]) {
		for(int i = 0; i<numArray.length; i++) {
			strArr += numArray[i] + " ";
		}
		strArr += "}";
		System.out.println(strArr + "\n");
	}

}
